# .Bashrc

Config file for bash terminal.


## How to

### Clone the project
    $ git clone linktorepo

### Delete old config (remember to backup)
    $ rm ~/.bashrc

### Create symlink
    $ ln -s ~/bashrc/.bashrc ~/.bashrc

