#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


alias ls='ls --color=auto'
alias carla="ollama run carla"
alias dolores="ollama run dolores"
alias prodvpn="sudo openvpn --config /etc/ngi-vpn/prod/vgw-shared-prod/vpnconfig_prod.ovpn"
alias testvpn="sudo openvpn --config /etc/ngi-vpn/test/vgw-shared-test/vpnconfig_test.ovpn"
alias rm="trash"
alias vim="nvim"
alias code='code --enable-proposed-api GitHub.copilot'
alias aks-geohub-test="kubectl config use-context aks-geohub-test"
alias test="kubectl config use-context aks-geohub-test && sudo openvpn --config /etc/ngi-vpn/test/vgw-shared-test/vpnconfig_test.ovpn"
alias aks-geohub-prod="kubectl config use-context aks-geohub-prod"
alias prod="kubectl config use-context aks-geohub-prod && sudo openvpn --config /etc/ngi-vpn/prod/vgw-shared-prod/vpnconfig_prod.ovpn"
alias noise="play -n synth pinknoise synth brownnoise mix"
alias vimconf="nvim ~/.config/nvim/init.lua"
alias bashconf="nvim ~/.bashrc"
alias spot="spt"
alias wingardium-leviosa="mv"
#alias avada-cadabra="rm -r"
alias expelliarmus="sudo rm -r"
alias citrix="/opt/Citrix/ICAClient/wfica"
alias teamsx="pkill -f 'microsoft.*teams|teams.*microsoft'"
alias avada-cadabra="pkill -f 'microsoft.*teams|teams.*microsoft'"
alias record="recordmydesktop"
alias powah="sudo powertop"
alias alohomora="sudo systemctl restart systemd-resolved"
alias w="watch -n 0.5"
alias news="newsraft -f ~/.config/newsraft-config/feed"
alias ss5="simplicitystudio5"

PS1='[\u@\h \W]\$ '

# Setting history size
HISTSIZE=100000
HISTFILESIZE=50000

export PATH=$PATH:/usr/local/bin
export EDITOR=nvim

#source ncs//nrf_connect_sdk/zephyr/zephyr-env.sh
#source ncs//nrf_connect_sdk/zephyr/zephyr-env.sh


# Created by `pipx` on 2023-09-26 07:10:51
export PATH="$PATH:/home/heigre/.local/bin"
eval "$(register-python-argcomplete pipx)" # set up bash completion for pipx
